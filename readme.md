#CURRENT STATUS: IN DEVELOPMENT / UNSTABLE

#Simple Asset Collections for Laravel 4

A Laravel 4 package providing an ultra basic Asset Manager similar to Laravel 3.

##Installation

Via composer / packagist - in composer.json require...

    ::php
    "codenamegary/l4blog": "dev-master"

##Example usage:

###Step 1: Resolve a Shared Instance from the IoC Container

If you have a $app within context you can resolve like so...

    :::php
    $assets = $app['l4sacs'];

If no $app is in context, just call the App facade...

    :::php
    $assets = App::make('l4sacs');

###Step 2: Add Assets and Collections

    :::php
    // Add a stylesheet to the default collection
    $assets->addCss(asset('css/styles.css'));
    // Add a stylesheet to the header collection, if no 'header'
    // collection exists it will be automatically created.
    $assets->collection('header')->addCss(asset('css/styles.css'));
    // Add a javascript source to the footer collection
    $assets->collection('footer')->addJs(asset('js/footerjs.js'));

###Step 3: Pass the Assets to your View

    :::php
    $header = View::make('header')->with('assets',$assets);

###Step 4: Render Assets inside your View

    :::php
    // Render stylesheet elements for the 'header' collection
    $assets->collection('header')->render('css');
    // Render script elements for the footer collection
    $assets->collection('footer')->render('js');
    // Or recursively render everything, including any sub-collections
    $assets->render('css',true);
    $assets->render('js',true);
