<?php namespace Codenamegary\L4sacs;

/**
 * Represents a group of Assets. Shared instance provided through IoC by default - 'l4sacs'. 
 *  
 * @package Codenamegary
 * @subpackage L4sacs
 * @author Gary Saunders <gary@powrit.com>
 * @license MIT
 * @license http://opensource.org/licenses/MIT
 */
class AssetCollection {
    
    /**
     * The name of the collection 
     * @var string
     */
    protected $name;
    
    /**
     * Sub-collections
     * @var array
     */
    protected $collections = array();

    /**
     * Assets belonging to this collection 
     * @var array
     */
    protected $assets = array(
        'css'   => array(),
        'js'    => array(),
    );
    
    /**
     * @param   string  $name The name of the collection, used as a key in the $collections property.
     */
    public function __construct( $name = '' )
    {
        $this->name = $name;
    }
    
    
    /**
     * Add a CSS asset to the collection.
     * 
     * @param   string  $url The fully qualified public URL of the asset. 
     */
    public function addCss( $url )
    {
        $this->assets[ 'css' ][] = new Asset( $url, 'css' );
    }

    /**
     * Add a JS asset to the collection.
     * 
     * @param   string  $url The fully qualified public URL of the asset. 
     */
    public function addJs( $url )
    {
        $this->assets[ 'js' ][] = new Asset( $url, 'js' );
    }
    
    /**
     * Adds an existing instance of AssetCollection as a nested collection.
     * 
     * @param   AssetCollection $assetCollection    An existing AssetCollection instance 
     */
    public function addCollection( AssetCollection $assetCollection )
    {
        $this->collections[ $assetCollection->name ] = $assetCollection;
    }

    /**
     * Renders all of the assets belonging to the collection, recurse disabled by default.
     * 
     * @param   string  $type       Either 'css' or 'js'
     * @param   boolean $recurse    Recurse each sub-collection and render their results, default = false.
     */
    public function render( $type = 'css', $recurse = false )
    {
        $output = '';

        if($recurse === true)
            foreach( $this->collections as $collection )
            {
                $output .= $collection->render( $type, true );
            }

        foreach( $this->assets[ $type ] as $asset )
        {
            $output .= $asset->render();
        }

        return $output;
    }
    
    /**
     * Initializes and returns a new sub-collction with $name or returns the existing sub-collection of $name.
     * 
     * @param   string  $name   The name of the collection, will be used as a key on the $collections property.
     */
    public function collection( $name )
    {
        if( !isset( $this->collections[ $name ] ) ) $this->collections[ $name ] = new AssetCollection( $name );
        return $this->collections[ $name ];
    }
    
}
