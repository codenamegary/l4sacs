<?php namespace Codenamegary\L4sacs;
/**
 * A wrapper / element renderer for an individual CSS or JavaScript asset. 
 * 
 * @package Codenamegary
 * @subpackage L4sacs
 * @author Gary Saunders <gary@powrit.com>
 * @license MIT
 * @license http://opensource.org/licenses/MIT
 */
class Asset {
    
    /**
     * 'css' or 'js'
     * @var string
     */
    protected $type;
    
    /**
     * The fully qualified public URL to the asset.
     * @var string
     */
    protected $url;
    
    /**
     * Simple array of strings to be used for merging and rendering each element type
     * @var array
     */
    protected $render = array(
        'css'   => '<link href="%s" rel="stylesheet" type="text/css">',
        'js'    => '<script type="text/javascript" src="%s"></script>',
    );

    /**
     * Create a new asset wrapper.
     *
     * @param   string  $url The fully qualified public URL of the asset
     * @param   string  $type The type of asset, currently must be 'css' or 'js'
     */
    public function __construct( $url, $type = 'css' )
    {
        $this->url = $url;
        $this->type = $type;
    }
    
    /**
     * Merges the URL into the render string and appends a crlf to look pretty in outputted HTML source.
     */
    public function render()
    {
        return sprintf( $this->render[ $this->type ], $this->url ) . "\r\n";
    }
    
}
