<?php namespace Codenamegary\L4sacs;

use Illuminate\Support\ServiceProvider;

/**
 * @package Codenamegary
 * @subpackage L4sacs
 * @author Gary Saunders <gary@powrit.com>
 * @license MIT
 * @license http://opensource.org/licenses/MIT
 */
class L4sacsServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('codenamegary/l4sacs');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAssetCollection();
    }

    public function registerAssetCollection()
    {
        // Binds a shared instance of L4sacs to the IoC
        $this->app['l4sacs'] = $this->app->share(function($app)
        {
            return new \Codenamegary\L4sacs\AssetCollection('assets');
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('l4sacs');
    }

}